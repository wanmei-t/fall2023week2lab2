//Wan Mei Tao
package mainPackage;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args)
    {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Trek", 7, 60.9);
        bicycles[1] = new Bicycle("Giant", 30, 50.4);
        bicycles[2] = new Bicycle("Decathlon", 7, 40.3);
        bicycles[3] = new Bicycle("Trek", 18, 55.8);

        for (int i = 0; i < bicycles.length; i++)
        {
            System.out.println("\nBike " + (i+1));
            System.out.println(bicycles[i]);
        }
    }
}
